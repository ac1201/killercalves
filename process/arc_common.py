import numpy as np
import matplotlib.pyplot as plt
plt.clf()
plt.ion()
from scipy import optimize
from numpy import sqrt



def power_law_plot(xdata,ydata):
    plt.loglog(xdata,ydata)
    logx = np.log10(xdata[0:])
    logy = np.log10(ydata[0:])
    yerr = 1.0
    logyerr = yerr / ydata
    # define our (line) fitting function
    fitfunc = lambda p, x: p[0] + p[1] * x
    errfunc = lambda p, x, y, err: (y - fitfunc(p, x)) / err
    pinit = [1.0, -1.0]
    out = optimize.leastsq(errfunc, pinit,
               args=(logx, logy, logyerr), full_output=1)
    pfinal = out[0]
    covar = out[1]
    print pfinal
    print covar
    index = pfinal[1]
    amp = 10.0**pfinal[0]
    indexErr = sqrt( covar[0][0] )
    ampErr = sqrt( covar[1][1] ) * amp
