import csv
import json
import sys

tree = {}

dpath = '../DATA/DATA/code_mapping.csv'

reader = csv.reader(open(dpath, 'rb'))
reader.next() 
for row in reader:
    subtree = tree
    for i, cell in enumerate(row):
        if cell:
            if cell not in subtree:
                subtree[cell] = {} if i<len(row)-1 else 1
            subtree = subtree[cell]

print json.dumps(tree, indent=4)
