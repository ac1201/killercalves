""" Calculates a histogram of funding by institute. """

import numpy as np
import csv

dpath = '../DATA/funding-institutes.csv'

institution = []
amount = []

with open(dpath,'rb') as csvfile:
    reader = csv.reader(csvfile,delimiter=',')
    i = 0
    for row in reader:
        #print ','.join(row)
        if i > 0:
            institution.append(row[0]) 
            amount.append(row[1]) 
        i += 1


import numpy as np

amount.sort()
amount = np.array(amount,np.float)
institution = np.array(institution)
bins = np.arange(0.1,amount.max(),amount.max()/10.)
histed = np.histogram(amount,bins=bins)[0]

#plt.hist(amount,bins=bins)
#idx = np.argsort(amount)
#amount = amount[idx]

#idx = histed > 0.0
#amount = amount[idx]
#histed = histed[idx]

xdata = histed
ydata = bins[1:]

from arc_common import power_law_plot
power_law_plot(xdata,ydata)




        
        
