""" Creates a bar plot of funding per institute, sorted by amount. """

import numpy as np
import csv

dpath = '../DATA/funding-institutes.csv'

institution = []
amount = []

with open(dpath,'rb') as csvfile:
    reader = csv.reader(csvfile,delimiter=',')
    i = 0
    for row in reader:
        #print ','.join(row)
        if i > 0:
            institution.append(row[0]) 
            amount.append(row[1]) 
        i += 1

from scipy import optimize
import matplotlib.pyplot as plt
plt.clf()
plt.ion()
plt.axes([0.1,0.5,0.6,0.4])

amount = np.array(amount[::-1],np.float)
institution = np.array(institution)
idx = np.argsort(amount)
amount = amount[idx]
institution = institution[idx]

import numpy as np
left = np.arange(amount.size)
plt.bar(left,amount[::-1])
ax = plt.gca()
ax.set_xticks(left)
ax.set_xticklabels(institution,rotation='vertical')


