data_type =  [('PID','S9'),('Scheme',Object),('syear','i4'),('cyear','i4')
              ,('lat','<f8'),('lon','<f8'),('pressure','<f8')]

dpath = '../data/names.csv'
dfile = open(dpath)

n = 0
for line in dfile.readlines():
    print line
    n += 1
    ofile.write(line)
    if n == 1000:
        break 

ofile.close()

#
def load_csv(fname):
    """ Load tropical cyclone tracks from a csv file given by
        fname, which must conform to the data schema.
    """
    tk = np.genfromtxt(fname,delimiter=',',skip_header=1,
            dtype=data_type)
        # Was previously necessary, may be necessary in future:
        # Strip the extraneous quotation marks
        #for i in range(shd['name'].size):
        #    shd['name'][i] = shd['name'][i][1:-1]
    return tk DP130100300


