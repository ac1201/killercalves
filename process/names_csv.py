""" Network analysis of researcher names.
"""

import csv
import networkx as nx
from scipy.spatial import KDTree, cKDTree

# 1. split the data up into good and problem
#
# 1.1 ONLY USE ONES THAT WORK
dpath = '../DATA/names.csv'

project = []
names = []

salutations = ['A//Prof','Prof','Dr','Mr','Ms','Mrs']
def clean_names(namestr):
    """ returns a cleaned list of names """
    namelist = (namestr.split(';'))
    names_stripped = []
    for name in namelist:
        name = name.lstrip()  
        name = name.lstrip()  
        for salutation in salutations:
            name = name.lstrip(salutation)
        name = name.lstrip()
        if len(name) > 0:
            names_stripped.append(name)
    return names_stripped

def isok(namestr):
    """ is the name list well behaved? """
    if ';' in namestr:
        return True

allnames = []
projnames = {}
# Read in the list of project participants
with open(dpath,'rb') as csvfile:
    reader = csv.reader(csvfile,delimiter=',')
    i = 0
    for row in reader:
        #print ','.join(row)
        if i > 0:
            project.append(row[0])
            namestr = row[1]
            if isok(namestr):
                namelist = clean_names(namestr)
                projnames[row[0]] = namelist
            
                for name in namelist: 
                    allnames.append(name)

        i += 1

unique_names = list(set(allnames))

connections = []

import os
import numpy as np

if not os.path.exists('nlist.npy'):
    i = 0
    for proj in projnames.keys():
        pmems = projnames[proj]
        print i
        for mem1 in pmems:
            mem1dx = unique_names.index(mem1)
            for mem2 in pmems:
                mem2dx = unique_names.index(mem2)
                if mem1 != mem2:
                    connections.append((mem1dx,mem2dx))
        i += 1
    C = np.array(connections)
    np.save('nlist.npy',C)
else:
    C = np.load('nlist.npy')

G = nx.from_edgelist(list(C))#connections)

# CLUSTERING
clust = nx.clustering(G)
clu = []
for k in clust.keys():
    clu.append(clust[k])

import matplotlib.pyplot as plt
plt.hist(clu[:])






