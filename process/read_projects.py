data_type =  [('PID','S9'),('Scheme','S2'),('syear','i4'),('cyear','i4')
              ,('lat','<f8'),('lon','<f8'),('pressure','<f8')]


Project ID
Scheme
Submission
Year
Commencement Year
Administering
Organisation
State Territory
Investigators
Project Title
National/Community
Benefit	
Primary FoR/RFCD	
Primary FoR/RFCD Description
Funding Award	2002	2003	2004	2005	2006	2007	2008	2009	2010	2011	2012	2013	2014	2015	2016	2017	2018


# TODO set to environment variable
ddir = '/Users/acharles/work/shtc/data'
#ddir='/flurry/home/acharles/shtc'

def load_csv(fname):
    """ Load tropical cyclone tracks from a csv file given by
        fname, which must conform to the data schema.
    """
    tk = np.genfromtxt(fname,delimiter=',',skip_header=1,
            dtype=data_type)
        # Was previously necessary, may be necessary in future:
        # Strip the extraneous quotation marks
        #for i in range(shd['name'].size):
        #    shd['name'][i] = shd['name'][i][1:-1]
    return tk


